export const AppConfig = {
  routes: {
    createOrder: 'create-order',
    orderList: 'orders',
    uploadInvoice: 'upload-invoice',
    invoiceList: 'invoices',
    dashboard: 'dashboard',
    deliverySchedule: 'delivery-schedule',
    stoketake: 'stoketake',
    myProducts: 'my-products',
    mySuppliers: 'my-suppliers',
    directory: 'directory',
    reports: 'reports',
    settings: 'settings',
  },
  request: {
    endpoints: {
      base: 'https://food-razor.herokuapp.com',
    },
  },
};

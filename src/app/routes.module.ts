import {NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {CreateOrderComponent} from './core/components/create-order/create-order.component';
import {OrderListComponent} from './core/components/order-list/order-list.component';
import {UploadInvoiceComponent} from './core/components/upload-invoice/upload-invoice.component';
import {InvoiceListComponent} from './core/components/invoice-list/invoice-list.component';
import {DashboardComponent} from './core/components/dashboard/dashboard.component';
import {DeliveryScheduleComponent} from './core/components/delivery-schedule/delivery-schedule.component';
import {StoketakeComponent} from './core/components/stoketake/stoketake.component';
import {MyProductsComponent} from './core/components/my-products/my-products.component';
import {MySuppliersComponent} from './core/components/my-suppliers/my-suppliers.component';
import {DirectoryComponent} from './core/components/directory/directory.component';
import {ReportsComponent} from './core/components/reports/reports.component';
import {SettingsComponent} from './core/components/settings/settings.component';

import {AppConfig} from './config/app.config';

const routes: Routes = [
  { path: AppConfig.routes.createOrder, component: CreateOrderComponent, },
  { path: AppConfig.routes.orderList, component: OrderListComponent, },
  { path: AppConfig.routes.uploadInvoice, component: UploadInvoiceComponent, },
  { path: AppConfig.routes.invoiceList, component: InvoiceListComponent, },
  { path: AppConfig.routes.dashboard, component: DashboardComponent, },
  { path: AppConfig.routes.deliverySchedule, component: DeliveryScheduleComponent, },
  { path: AppConfig.routes.stoketake, component: StoketakeComponent, },
  { path: AppConfig.routes.myProducts, component: MyProductsComponent, },
  { path: AppConfig.routes.mySuppliers, component: MySuppliersComponent, },
  { path: AppConfig.routes.directory, component: DirectoryComponent, },
  { path: AppConfig.routes.reports, component: ReportsComponent, },
  { path: AppConfig.routes.settings, component: SettingsComponent, },
  { path: '**', redirectTo: AppConfig.routes.createOrder },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}

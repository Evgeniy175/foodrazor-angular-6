export enum Styles {
  Grey,
  GreyFilled,
  Green,
  GreenFilled,
  FooterGrey,
  FooterGreen,
}

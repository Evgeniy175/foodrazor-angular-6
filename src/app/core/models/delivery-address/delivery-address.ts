export class DeliveryAddress {
  public address: string;
  public mainEmail: string;
  public phoneNumber: string;
  public ccEmails: string[];
}
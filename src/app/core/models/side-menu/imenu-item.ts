export interface IMenuItem {
  title?: string;
  uri?: string;
  iconBefore?: string;
  iconAfter?: string;
}

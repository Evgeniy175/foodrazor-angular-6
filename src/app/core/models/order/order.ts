export class Order {
  public id: any;
  public orderNumber: string;
  public deliveryDate: string;
  public total: number;
}

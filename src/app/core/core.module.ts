import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {RouterModule} from '@angular/router';
import {BrowserModule} from "@angular/platform-browser";
import {AngularFontAwesomeModule} from 'angular-font-awesome';

import {SharedModule} from '../shared/shared.module';

import {SideMenuComponent} from './components/side-menu/side-menu.component';
import {NavBarComponent} from './components/navbar/navbar.component';
import {CreateOrderComponent} from './components/create-order/create-order.component';
import {OrderListComponent} from './components/order-list/order-list.component';
import {UploadInvoiceComponent} from './components/upload-invoice/upload-invoice.component';
import {InvoiceListComponent} from './components/invoice-list/invoice-list.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {DeliveryScheduleComponent} from './components/delivery-schedule/delivery-schedule.component';
import {StoketakeComponent} from './components/stoketake/stoketake.component';
import {MyProductsComponent} from './components/my-products/my-products.component';
import {MySuppliersComponent} from './components/my-suppliers/my-suppliers.component';
import {DirectoryComponent} from './components/directory/directory.component';
import {ReportsComponent} from './components/reports/reports.component';
import {SettingsComponent} from './components/settings/settings.component';

import {HttpService} from './services/http/http.service';

import {NotificationCountPipe} from './pipes/notification-count/notification-count.pipe';

@NgModule({
  declarations: [
    SideMenuComponent,
    NavBarComponent,
    CreateOrderComponent,
    OrderListComponent,
    UploadInvoiceComponent,
    InvoiceListComponent,
    DashboardComponent,
    DeliveryScheduleComponent,
    StoketakeComponent,
    MyProductsComponent,
    MySuppliersComponent,
    DirectoryComponent,
    ReportsComponent,
    SettingsComponent,

    NotificationCountPipe
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    AngularFontAwesomeModule,
    SharedModule,
  ],
  exports: [
    SideMenuComponent,
    NavBarComponent,
    CreateOrderComponent,
  ],
  providers: [
    HttpService,
  ],
})

export class CoreModule {}

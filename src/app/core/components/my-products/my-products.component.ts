import { Component } from '@angular/core';

@Component({
  selector: 'my-products',
  templateUrl: 'my-products.component.html',
  styleUrls: ['my-products.component.scss']
})
export class MyProductsComponent {}

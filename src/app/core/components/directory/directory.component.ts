import { Component } from '@angular/core';

@Component({
  selector: 'directory',
  templateUrl: 'directory.component.html',
  styleUrls: ['directory.component.scss']
})
export class DirectoryComponent {}

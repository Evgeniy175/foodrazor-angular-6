import {Component, OnInit} from '@angular/core';


import {User} from "../../models/user/user";
import {Company} from "../../models/company/company";

@Component({
  selector: 'foodrazor-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavBarComponent implements OnInit{
  public notificationsCounter: number = 4;
  public user: User = new User();

  public isCompaniesExpanded = false;

  public selectedCompany: Company;
  public selectedCompanyId: any = 'c1';
  public companies: Company[] = [
    {
      id: 'hc',
      name: 'Holding Company',
    },
    {
      id: 'c1',
      name: 'Company Outlet A',
    },
    {
      id: 'c2',
      name: 'Company Outlet B',
    },
    {
      id: 'c3',
      name: 'Company Outlet C',
    },
    {
      id: 'c4',
      name: 'Company Outlet D',
    },
    {
      id: 'c5',
      name: 'Company Outlet E',
    },
    {
      id: 'c6',
      name: 'Company Outlet F',
    },
    {
      id: 'c7',
      name: 'Company Outlet G',
    },
    {
      id: 'c8',
      name: 'Company Outlet H',
    },
  ];

  ngOnInit(): void {
    // this method used for mock data

    this.user.image = "assets/images/profile_icon.jpg";
    this.user.name = "Mr. Bean";

    this.initSelectedCompany();
  }

  onCompaniesExpand() {
    this.isCompaniesExpanded = !this.isCompaniesExpanded;
  }

  onCompaniesCollapse() {
    this.isCompaniesExpanded = false;
  }

  onCompanyClick(id) {
    this.selectedCompanyId = id;
    this.initSelectedCompany();
    this.isCompaniesExpanded = false;
  }

  initSelectedCompany() {
    this.selectedCompany = this.companies.find(company => company.id === this.selectedCompanyId);
  }
}

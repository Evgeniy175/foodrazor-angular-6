import { Component } from '@angular/core';

import {IMenuItem} from '../../models/side-menu/imenu-item';

import {AppConfig} from '../../../config/app.config';

@Component({
  selector: 'foodrazor-side-menu',
  templateUrl: 'side-menu.component.html',
  styleUrls: ['side-menu.component.scss']
})
export class SideMenuComponent {
  public topItems: IMenuItem[] = [
    {
      title: 'Create Order',
      uri: AppConfig.routes.createOrder,
      iconBefore: 'cart-plus',
    },
    {
      title: 'Order List',
      uri: AppConfig.routes.orderList,
      iconBefore: 'edit',
    },
  ];
  public bottomItems: IMenuItem[] = [
    {
      title: 'Upload Invoice',
      uri: AppConfig.routes.uploadInvoice,
      iconBefore: 'newspaper-o',
    },
    {
      title: 'Invoice List',
      uri: AppConfig.routes.invoiceList,
      iconBefore: 'file-text',
    },
    {
      title: 'Dashboard',
      uri: AppConfig.routes.dashboard,
      iconBefore: 'bar-chart',
    },
    {
      title: 'Delivery Schedule',
      uri: AppConfig.routes.deliverySchedule,
      iconBefore: 'calendar-o',
    },
    {
      title: 'Stoketake',
      uri: AppConfig.routes.stoketake,
      iconBefore: 'list',
    },
    {
      title: 'My Products',
      uri: AppConfig.routes.myProducts,
      iconBefore: 'dropbox',
    },
    {
      title: 'My Suppliers',
      uri: AppConfig.routes.mySuppliers,
      iconBefore: 'truck',
    },
    {
      title: 'Directory',
      uri: AppConfig.routes.directory,
      iconBefore: 'book',
    },
    {
      title: 'Reports',
      uri: AppConfig.routes.reports,
      iconBefore: 'pie-chart',
    },
    {
      title: 'Settings',
      uri: AppConfig.routes.settings,
      iconBefore: 'cog',
      iconAfter: 'chevron-right',
    },
  ];
}

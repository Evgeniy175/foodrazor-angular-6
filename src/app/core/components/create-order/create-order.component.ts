import {Component} from '@angular/core';

import {Supplier} from "../../models/supplier/supplier";
import {DeliveryAddress} from "../../models/delivery-address/delivery-address";

import {Styles} from '../../models/styles/styles';

@Component({
  selector: 'create-order',
  templateUrl: 'create-order.component.html',
  styleUrls: ['create-order.component.scss']
})
export class CreateOrderComponent {
  selectedSupplier: Supplier;
  deliveryAddress: DeliveryAddress = new DeliveryAddress();

  get styles(): typeof Styles {
    return Styles;
  }

  constructor() {
    this.deliveryAddress.address = '30 alexandra lane #04-03 skylight building singapore 119982';
    this.deliveryAddress.mainEmail = 'username@email.com';
    this.deliveryAddress.phoneNumber = '98876754';
    this.deliveryAddress.ccEmails = [
      'evgeniy@senior-node.com',
      'santa@senior-node.com',
      'hohoho@senior-node.com',
    ];
  }

  onSupplierSelect(supplier: Supplier) {
    this.selectedSupplier = supplier;
  }
}

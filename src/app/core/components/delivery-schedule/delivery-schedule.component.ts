import { Component } from '@angular/core';

@Component({
  selector: 'delivery-schedule',
  templateUrl: 'delivery-schedule.component.html',
  styleUrls: ['delivery-schedule.component.scss']
})
export class DeliveryScheduleComponent {}

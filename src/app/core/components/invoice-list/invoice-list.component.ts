import { Component } from '@angular/core';

@Component({
  selector: 'invoice-list',
  templateUrl: 'invoice-list.component.html',
  styleUrls: ['invoice-list.component.scss']
})
export class InvoiceListComponent {}

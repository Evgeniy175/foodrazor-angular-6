import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable()
export class HttpService {
  public constructor(private http: HttpClient,
                     private router: Router) {
  }
}

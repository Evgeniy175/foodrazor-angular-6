import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notificationCount'
})
export class NotificationCountPipe implements PipeTransform {
  transform(value: number): string {
    if (value === 0) return;
    return value > 0 && value < 10 ? value.toString() : '!';
  }
}
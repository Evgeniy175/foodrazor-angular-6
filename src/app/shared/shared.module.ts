import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from '@angular/forms';
import {AngularFontAwesomeModule} from "angular-font-awesome";

import {SupplierSelectComponent} from './components/supplier-select/supplier-select.component';
import {RecentOrderSelectComponent} from './components/recent-order-select/recent-order-select.component';
import {DropdownCloseComponent} from './components/dropdown-close/dropdown-close.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {StepNumberComponent} from './components/step-number/step-number.component';
import {OtherInstructionsTextComponent} from './components/other-instructions-text/other-instructions-textarea.component';
import {PrintComponent} from './components/print/print.component';
import {DateSelectComponent} from './components/date-select/date-select.component';
import {AddProductComponent} from './components/add-product/add-product.component';
import {ButtonComponent} from './components/button/button.component';
import {DeliveryAddressDropdownComponent} from './components/delivery-address-dropdown/delivery-address-dropdown.component';

@NgModule({
  declarations: [
    SupplierSelectComponent,
    RecentOrderSelectComponent,
    DropdownCloseComponent,
    PageHeaderComponent,
    StepNumberComponent,
    OtherInstructionsTextComponent,
    PrintComponent,
    DateSelectComponent,
    AddProductComponent,
    ButtonComponent,
    DeliveryAddressDropdownComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    AngularFontAwesomeModule,
  ],
  exports: [
    SupplierSelectComponent,
    RecentOrderSelectComponent,
    DropdownCloseComponent,
    PageHeaderComponent,
    StepNumberComponent,
    OtherInstructionsTextComponent,
    PrintComponent,
    DateSelectComponent,
    AddProductComponent,
    ButtonComponent,
    DeliveryAddressDropdownComponent,
  ],
})

export class SharedModule {}

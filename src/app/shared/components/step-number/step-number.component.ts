import {Component, Input} from '@angular/core';

@Component({
  selector: 'step-number',
  templateUrl: 'step-number.component.html',
  styleUrls: ['step-number.component.scss']
})
export class StepNumberComponent {
  @Input() isCompleted: boolean = false;
  @Input() isLast: boolean = false;
  @Input() number: number;
}

import {Component, Input} from '@angular/core';

import {DeliveryAddress} from "../../../core/models/delivery-address/delivery-address";

@Component({
  selector: 'delivery-address-dropdown',
  templateUrl: 'delivery-address-dropdown.component.html',
  styleUrls: ['delivery-address-dropdown.component.scss']
})
export class DeliveryAddressDropdownComponent {
  @Input() data: DeliveryAddress;
  isHovered = false;

  get isDropdownVisible() {
    return this.isHovered && !!this.data;
  }

  get address() {
    return this.isDropdownVisible && this.data.address;
  }

  get mainEmail() {
    return this.isDropdownVisible && this.data.mainEmail;
  }

  get phoneNumber() {
    return this.isDropdownVisible && this.data.phoneNumber;
  }

  get ccEmails() {
    return this.isDropdownVisible && this.data.ccEmails;
  }

  onMouseEnter() {
    this.isHovered = true;
  }

  onMouseLeave(e) {
    this.isHovered = this.hasParents(e.toElement || e.relatedTarget);
  }

  hasParents(elem: HTMLElement) {
    let parent: HTMLElement = elem.parentElement;
    while(!!parent) {
      if (parent.classList.contains('delivery-address-wrapper')) return true;
      parent = parent.parentElement;
    }
    return false;
  }
}

import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'dropdown-close',
  templateUrl: 'dropdown-close.component.html',
  styleUrls: ['dropdown-close.component.scss']
})
export class DropdownCloseComponent {
  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  click() {
    this.onClick.emit();
  }
}

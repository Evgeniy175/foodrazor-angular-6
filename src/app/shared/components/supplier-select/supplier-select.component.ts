
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {Subject} from 'rxjs';




import {Supplier} from "../../../core/models/supplier/supplier";

@Component({
  selector: 'supplier-select',
  templateUrl: 'supplier-select.component.html',
  styleUrls: ['supplier-select.component.scss']
})
export class SupplierSelectComponent implements OnInit, OnDestroy {
  isDropdownVisible = false;

  get selectPlaceholder(): string {
    return (this.selectedSupplier && this.selectedSupplier.name) || 'Select a supplier';
  }

  // todo move to total-text-formatter pipe
  get suppliersCounterText(): string {
    if (!this.suppliers || this.suppliers.length === 0) return 'No results';
    const { length } = this.suppliers;
    return `${length} supplier${length === 1 ? '' : 's'}`
  }

  @Input() inputPlaceholder: string = 'COCA-COLA';

  @Output() onSelect: EventEmitter<Supplier> = new EventEmitter();

  debounceTime: number = 250;
  initialSuppliers: Supplier[] = [
    {
      id: '1',
      name: 'COCA-COLA SINGAPORE BEVERAGES PTE LTD',
    },
    {
      id: '2',
      name: 'Minsk Airport Catering Service',
    },
    {
      id: '3',
      name: 'Wabash Foodservice',
    },
    {
      id: '4',
      name: 'Troyer Foods, Inc.',
    },
    {
      id: '5',
      name: 'Stanz Foodservice, Inc.',
    },
    {
      id: '6',
      name: 'McFarling Foods, Inc.',
    },
    {
      id: '7',
      name: 'Sysco Indianapolis, LLC',
    },
  ];

  inputValue: string;
  inputValueChanged: Subject<string>;

  suppliers: Supplier[];
  selectedSupplierId: any;
  selectedSupplier;

  ngOnInit() {
    this.inputValueChanged = new Subject<string>();
    this.suppliers = this.initialSuppliers;
    this.initSelectedSupplier();
    this.initDebouncer();
  }

  ngOnDestroy() {
    this.onSelect.emit(null);
    this.inputValue = null;
    this.inputValueChanged = null;
    this.suppliers = null;
    this.selectedSupplier = null;
    this.selectedSupplierId = null;
    this.isDropdownVisible = false;
  }

  initDebouncer() {
    this.inputValueChanged.pipe(debounceTime(this.debounceTime),
    distinctUntilChanged(),)
    .subscribe((text: string) => {
      const re = new RegExp(text, 'gi');
      this.inputValue = text;
      this.suppliers = this.initialSuppliers.filter(({name}) => re.test(name));
    });
  }

  onDropdownVisibilityToggle() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }

  onInputChange(text: string) {
    this.inputValueChanged.next(text);
  }

  onItemClick(id) {
    this.selectedSupplierId = id;
    this.initSelectedSupplier();
    this.isDropdownVisible = false;
  }

  onClose() {
    this.isDropdownVisible = false;
  }

  initSelectedSupplier() {
    this.selectedSupplier = this.suppliers.find(supplier => supplier.id === this.selectedSupplierId);
    this.onSelect.emit(this.selectedSupplier);
  }
}

import {Component, Input} from '@angular/core';

@Component({
  selector: 'other-instructions-textarea',
  templateUrl: 'other-instructions-textarea.component.html',
  styleUrls: ['other-instructions-textarea.component.scss']
})
export class OtherInstructionsTextComponent {
  @Input() rows: number = 5;
  @Input() isPlaceholderVisible: boolean = false;
  placeholderVisible = true;

  get isPlaceholderShown() {
    return this.isPlaceholderVisible && this.placeholderVisible;
  }

  placeholderData: string[] = [
    'Specific instructions and comments',
    'Specific instructions text',
    'Comments text',
    'Specific instructions and',
    'Specific instructions text',
  ];

  onPlaceholderClick() {
    this.placeholderVisible = false;
  }

  onTextAreaBlur(e) {
    this.placeholderVisible = !e.target.value;
  }
}

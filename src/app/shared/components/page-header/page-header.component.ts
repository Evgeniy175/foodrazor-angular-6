import {Component, Input} from '@angular/core';

import {Styles} from "../../../core/models/styles/styles";

@Component({
  selector: 'page-header',
  templateUrl: 'page-header.component.html',
  styleUrls: ['page-header.component.scss']
})
export class PageHeaderComponent {
  @Input() text: string;
  @Input() labelText: string;
  @Input() style: Styles = Styles.GreyFilled;

  private static stylesMapping = {
    [Styles.Grey]: 'grey',
    [Styles.GreyFilled]: 'grey-filled',
    [Styles.Green]: 'green',
    [Styles.GreenFilled]: 'green-filled',
  };

  get labelStyle() {
    return `label label-${PageHeaderComponent.stylesMapping[this.style]}`;
  }
}

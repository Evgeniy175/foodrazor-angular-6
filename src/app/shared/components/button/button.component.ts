import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Styles} from '../../../core/models/styles/styles';

@Component({
  selector: 'foodrazor-button',
  templateUrl: 'button.component.html',
  styleUrls: ['button.component.scss']
})
export class ButtonComponent {
  @Input() text: string;
  @Input() style: Styles = Styles.GreyFilled;
  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

  private static stylesMapping = {
    [Styles.Grey]: 'grey',
    [Styles.GreyFilled]: 'grey-filled',
    [Styles.Green]: 'green',
    [Styles.GreenFilled]: 'green-filled',
    [Styles.FooterGrey]: 'footer-grey',
    [Styles.FooterGreen]: 'footer-green',
  };

  get buttonStyle() {
    return `button-wrapper button-style-${ButtonComponent.stylesMapping[this.style]}`;
  }

  click() {
    this.onClick.emit();
  }
}

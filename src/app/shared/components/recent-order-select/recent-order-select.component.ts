import {Component, OnDestroy, OnInit} from '@angular/core';

import {Order} from "../../../core/models/order/order";

@Component({
  selector: 'recent-order-select',
  templateUrl: 'recent-order-select.component.html',
  styleUrls: ['recent-order-select.component.scss']
})
export class RecentOrderSelectComponent implements OnInit, OnDestroy {
  isDropdownVisible = false;

  get selectPlaceholder(): string {
    return (this.selectedItem && this.selectedItem.orderNumber) || 'Select Recent Order (optional)';
  }

  // todo move to total-text-formatter pipe
  get totalCounterText(): string {
    if (!this.items || this.items.length === 0) return 'No results';
    const { length } = this.items;
    return `${length} result${length === 1 ? '' : 's'}`
  }

  initialList: Order[] = [
    {
      id: '1',
      orderNumber: '18ABC0001',
      deliveryDate: '1 Apr 2018, Thu',
      total: 21,
    },
    {
      id: '2',
      orderNumber: '18ABC0002',
      deliveryDate: '2 Apr 2018, Thu',
      total: 41,
    },
    {
      id: '3',
      orderNumber: '18ABC0003',
      deliveryDate: '3 Apr 2018, Thu',
      total: 42,
    },
    {
      id: '4',
      orderNumber: '18ABC0004',
      deliveryDate: '4 Apr 2018, Thu',
      total: 82,
    },
    {
      id: '5',
      orderNumber: '18ABC0005',
      deliveryDate: '5 Apr 2018, Thu',
      total: 4,
    },
    {
      id: '6',
      orderNumber: '18ABC0006',
      deliveryDate: '6 Apr 2018, Thu',
      total: 42,
    },
    {
      id: '7',
      orderNumber: '18ABC0007',
      deliveryDate: '7 Apr 2018, Thu',
      total: 41,
    },
    {
      id: '8',
      orderNumber: '18ABC0008',
      deliveryDate: '8 Apr 2018, Thu',
      total: 24,
    },
    {
      id: '9',
      orderNumber: '18ABC0009',
      deliveryDate: '9 Apr 2018, Thu',
      total: 45,
    },
    {
      id: '10',
      orderNumber: '18ABC0010',
      deliveryDate: '10 Apr 2018, Thu',
      total: 102,
    },
    {
      id: '11',
      orderNumber: '18ABC0011',
      deliveryDate: '11 Apr 2018, Thu',
      total: 21,
    },
    {
      id: '12',
      orderNumber: '18ABC0012',
      deliveryDate: '12 Apr 2018, Thu',
      total: 41,
    },
    {
      id: '13',
      orderNumber: '18ABC0013',
      deliveryDate: '13 Apr 2018, Thu',
      total: 42,
    },
    {
      id: '14',
      orderNumber: '18ABC0014',
      deliveryDate: '14 Apr 2018, Thu',
      total: 82,
    },
    {
      id: '15',
      orderNumber: '18ABC0015',
      deliveryDate: '15 Apr 2018, Thu',
      total: 4,
    },
    {
      id: '16',
      orderNumber: '18ABC0016',
      deliveryDate: '16 Apr 2018, Thu',
      total: 42,
    },
    {
      id: '17',
      orderNumber: '18ABC0017',
      deliveryDate: '17 Apr 2018, Thu',
      total: 41,
    },
    {
      id: '18',
      orderNumber: '18ABC0018',
      deliveryDate: '18 Apr 2018, Thu',
      total: 24,
    },
    {
      id: '19',
      orderNumber: '18ABC0019',
      deliveryDate: '19 Apr 2018, Thu',
      total: 45,
    },
    {
      id: '20',
      orderNumber: '18ABC0020',
      deliveryDate: '20 Apr 2018, Thu',
      total: 102,
    },
  ];

  items: Order[];
  selectedId: any;
  selectedItem;

  ngOnInit() {
    this.items = this.initialList;
    this.initSelected();
  }

  ngOnDestroy() {
    this.items = null;
    this.selectedItem = null;
    this.selectedId = null;
    this.isDropdownVisible = false;
  }

  onDropdownVisibilityToggle() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }

  onItemClick(id) {
    this.selectedId = id;
    this.initSelected();
    this.isDropdownVisible = false;
  }

  onClose() {
    this.isDropdownVisible = false;
  }

  initSelected() {
    this.selectedItem = this.items.find(item => item.id === this.selectedId);
  }
}
